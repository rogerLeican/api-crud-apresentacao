package com.technocorp.apicrudapresentacao.contrato.v1.cliente;

import com.technocorp.apicrudapresentacao.contrato.v1.cliente.mapeamento.ClienteMapeador;
import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.request.ClienteRequest;
import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.response.ClienteResponse;
import com.technocorp.apicrudapresentacao.impl.cliente.ClienteFacade;
import com.technocorp.apicrudapresentacao.impl.cliente.repositorio.ClienteEntidade;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
@AllArgsConstructor
public class ClienteContratoFacade {

    private ClienteFacade facade;

    List<ClienteEntidade> findAll(){
        return facade.findAll();
    }

    ClienteResponse findById(long id){
        return ClienteMapeador.mapaParaContrato(facade.findById(id));
    }

    ClienteResponse criar(ClienteRequest cliente){

        return ClienteMapeador.mapaParaContrato(facade.criar(ClienteMapeador.mapaParaImplementacao(cliente)));
    }
    ClienteResponse atualizar( ClienteRequest cliente) {

        return ClienteMapeador.mapaParaContrato(facade.atualizar(ClienteMapeador.mapaParaImplementacao(cliente)));
    }

    public ClienteResponse atualizarPart( ClienteRequest cliente) {
        return ClienteMapeador.mapaParaContrato( facade.atualizarPart(ClienteMapeador.mapaParaImplementacao(cliente)));
    }
    void deleteById(Long id){
         facade.deleteById(id);
    }


}

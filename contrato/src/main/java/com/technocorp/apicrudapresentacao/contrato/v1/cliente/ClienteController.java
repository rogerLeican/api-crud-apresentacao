package com.technocorp.apicrudapresentacao.contrato.v1.cliente;

import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.request.ClienteRequest;
import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.response.ClienteResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RequestMapping("/v1/clientes")
@RestController
@AllArgsConstructor

public class ClienteController {

    private final ClienteContratoFacade facade;


    @GetMapping("/teste")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(facade.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById( @Valid @PathVariable Long id) {
        return ResponseEntity.ok(facade.findById(id));
    }

    @PostMapping
    public ResponseEntity<?> criar(@Valid @RequestBody ClienteRequest cliente) {
        return new ResponseEntity<>(facade.criar(cliente),HttpStatus.CREATED);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable long id) {
       facade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping
    public ResponseEntity<?> atualizar(@RequestBody ClienteRequest cliente) {
        facade.atualizar(cliente);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping
    public ClienteResponse atualizarPart(@RequestBody ClienteRequest cliente) {
        return facade.atualizarPart(cliente);
    }

}

package com.technocorp.apicrudapresentacao.contrato.v1.cliente.handler;

import com.technocorp.apicrudapresentacao.impl.cliente.exception.ExceptionDetails;
import com.technocorp.apicrudapresentacao.impl.cliente.exception.ResourceNotFoundException;
import com.technocorp.apicrudapresentacao.impl.cliente.exception.ResourceNotFountDetails;
import com.technocorp.apicrudapresentacao.impl.cliente.exception.ValidationExceptionDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
    @ExceptionHandler(ResourceNotFoundException.class)

    public ResponseEntity<ResourceNotFountDetails> handlerRecursoNaoEncontradoException(ResourceNotFoundException excecao) {
        return new ResponseEntity<>(
                ResourceNotFountDetails.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.NOT_FOUND.value())
                        .title("Recurso não encontrado")
                        .detail(excecao.getMessage())
                        .developerMessage(excecao.getClass().getName())
                        .build(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)

    public ResponseEntity<ValidationExceptionDetails> handlerArgumentoNaoValidoException(
            MethodArgumentNotValidException excecao) {

        List<FieldError> ErrosDeCampo = excecao.getBindingResult().getFieldErrors();
        String campos = ErrosDeCampo.stream().map(FieldError::getField).collect(Collectors.joining(", "));
        String messagemDeCampo = ErrosDeCampo.stream().map(FieldError::getDefaultMessage).collect(Collectors.joining(", "));

        return new ResponseEntity<>(
                ValidationExceptionDetails.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .title("Erro na Validação do Campo")
                        .detail("Verifique o campo Babaixo")
                        .developerMessage(excecao.getClass().getName())
                        .Campo(campos)
                        .MessagemDeCampo(messagemDeCampo)
                        .build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExceptionDetails> handlerArgumentaNaoCompativel(
            MethodArgumentTypeMismatchException excecao) {

//        String error = excecao.getName() + "Deve ser do tipo" + excecao.getRequiredType().getName();

        return new ResponseEntity<>(
                ExceptionDetails.builder()
                        .timestamp(LocalDateTime.now())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .title("Erro no tipo definido")
                        .detail("Verifique o typo enviado")
                        .developerMessage(excecao.getClass().getName())
                        .build(), HttpStatus.BAD_REQUEST);
    }

}

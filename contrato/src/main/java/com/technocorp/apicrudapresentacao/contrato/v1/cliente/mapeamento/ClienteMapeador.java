package com.technocorp.apicrudapresentacao.contrato.v1.cliente.mapeamento;

import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.request.ClienteRequest;
import com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.response.ClienteResponse;
import com.technocorp.apicrudapresentacao.impl.cliente.modelo.ClienteModelo;

public class ClienteMapeador {

    public static ClienteResponse mapaParaContrato(ClienteModelo clienteModelo){
        return ClienteResponse.builder()
                .id(clienteModelo.getId())
                .nome(clienteModelo.getNome())
                .cpf(clienteModelo.getCpf())
                .build();
    }

    public static ClienteModelo mapaParaImplementacao(ClienteRequest clienteRequest){
        return ClienteModelo.builder()
                .id(clienteRequest.getId())
                .nome(clienteRequest.getNome())
                .cpf(clienteRequest.getCpf())
                .build();
    }

}

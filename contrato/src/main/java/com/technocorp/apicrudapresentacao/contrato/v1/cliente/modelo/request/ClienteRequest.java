package com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.NonNullFields;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClienteRequest {

    private Long id;
    @NotEmpty(message = "O nome é Obrigatorio")
    private String nome;
    @NotNull
    private String cpf;
}

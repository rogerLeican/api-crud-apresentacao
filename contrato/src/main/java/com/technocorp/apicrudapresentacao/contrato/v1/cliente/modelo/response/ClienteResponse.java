package com.technocorp.apicrudapresentacao.contrato.v1.cliente.modelo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ClienteResponse  {

    private long id;
    private String nome;
    private String cpf;

}

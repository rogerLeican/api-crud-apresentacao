package com.technocorp.apicrudapresentacao.impl.cliente;

import com.technocorp.apicrudapresentacao.impl.cliente.modelo.ClienteModelo;
import com.technocorp.apicrudapresentacao.impl.cliente.repositorio.ClienteEntidade;
import com.technocorp.apicrudapresentacao.impl.cliente.servico.ClienteServico;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ClienteFacade {

    private ClienteServico servico;

    public List<ClienteEntidade> findAll(){
        return servico.findAll();
    }
    
    public ClienteModelo findById(Long id){
        return servico.findById(id);
    }

    public ClienteModelo criar(ClienteModelo cliente){
        return servico.criar(cliente);
    }

    public void deleteById(Long id){
         servico.deleteById(id);
    }

     public ClienteModelo atualizar( ClienteModelo cliente) {
         return servico.atualizar(cliente);
    }

    public ClienteModelo atualizarPart( ClienteModelo cliente) {
        return servico.atualizarPart(cliente);
    }


}

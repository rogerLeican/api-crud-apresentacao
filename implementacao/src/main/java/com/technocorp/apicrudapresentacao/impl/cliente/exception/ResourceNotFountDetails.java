package com.technocorp.apicrudapresentacao.impl.cliente.exception;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
//@Getter
public class ResourceNotFountDetails extends ExceptionDetails {

    private String title;
    private int status;
    private String detail;
    private LocalDateTime timestamp;
    private String developerMessage;
}



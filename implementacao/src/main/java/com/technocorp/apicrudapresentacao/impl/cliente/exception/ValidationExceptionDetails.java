package com.technocorp.apicrudapresentacao.impl.cliente.exception;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ValidationExceptionDetails extends ExceptionDetails {

    private String Campo;
    private String MessagemDeCampo;

}

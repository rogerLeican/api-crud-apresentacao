package com.technocorp.apicrudapresentacao.impl.cliente.mapeador;
import com.technocorp.apicrudapresentacao.impl.cliente.modelo.ClienteModelo;
import com.technocorp.apicrudapresentacao.impl.cliente.repositorio.ClienteEntidade;


public class ClienteMapeador {

    //Passa os atributos do objeto ENTIDADE  para o objeto MODELO
    public static ClienteModelo mapaParaModelo(ClienteEntidade clienteEntidade) {
        return ClienteModelo.builder()
                .id(clienteEntidade.getId())
                .nome(clienteEntidade.getNome())
                .cpf(clienteEntidade.getCpf())
                .build();
    }
    //Faz o oposto
    //passa os atributos do objeto MODELO  para o objeto ENTIDADE
    public static ClienteEntidade mapaParaEntidade(ClienteModelo clienteModelo){
        return ClienteEntidade.builder()
                .id(clienteModelo.getId())
                .nome(clienteModelo.getNome())
                .cpf(clienteModelo.getCpf())
                .build();
    }

}

package com.technocorp.apicrudapresentacao.impl.cliente.modelo;

import com.technocorp.apicrudapresentacao.impl.cliente.servico.GeradorId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClienteModelo{
    private Long id;
    private String nome;
    private String cpf;
}

package com.technocorp.apicrudapresentacao.impl.cliente.servico;

import com.technocorp.apicrudapresentacao.impl.cliente.exception.ResourceNotFoundException;
import com.technocorp.apicrudapresentacao.impl.cliente.mapeador.ClienteMapeador;
import com.technocorp.apicrudapresentacao.impl.cliente.modelo.ClienteModelo;
import com.technocorp.apicrudapresentacao.impl.cliente.repositorio.ClienteEntidade;
import com.technocorp.apicrudapresentacao.impl.cliente.repositorio.ClienteRepositorio;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class ClienteServico {

    private ClienteRepositorio repositorio;

    public List<ClienteEntidade> findAll() {
        return repositorio.findAll();
    }


    public ClienteModelo findById(Long id) {

        return ClienteMapeador.mapaParaModelo(verifyIfExist(id));
    }

    public ClienteModelo criar(ClienteModelo cliente) {

//        try {
//            VerifyIfExistFieldNull(cliente);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
        return ClienteMapeador.mapaParaModelo(repositorio.save(ClienteMapeador.mapaParaEntidade(cliente)));
    }

    public void deleteById(Long id) {
        repositorio.delete(verifyIfExist(id));
    }

    public ClienteModelo atualizar(ClienteModelo cliente) {
        verifyIfExist(cliente.getId());
        return ClienteMapeador.mapaParaModelo(repositorio.save(ClienteMapeador.mapaParaEntidade(cliente)));
    }

    public ClienteModelo atualizarPart(ClienteModelo cliente) {

        verifyIfExist(cliente.getId());

        return ClienteMapeador.mapaParaModelo(repositorio.save(ClienteEntidade.builder()
                .id(cliente.getId())
                .nome(Objects.isNull(cliente.getNome()) ?
                        ClienteMapeador.mapaParaEntidade(findById(cliente.getId())).getNome() :
                        cliente.getNome())
                .cpf(Objects.isNull(cliente.getCpf()) ?
                        ClienteMapeador.mapaParaEntidade(findById(cliente.getId())).getCpf() :
                        cliente.getCpf())
                .build()));
    }

    private ClienteEntidade verifyIfExist(long id) {
        return repositorio.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Cliente não Encontrado"));
    }

//    private void VerifyIfExistFieldNull(ClienteModelo cliente) throws IllegalAccessException {
//
//        Class<ClienteModelo> clienteClass = ClienteModelo.class;
//        Field[] fields = clienteClass.getDeclaredFields();
//        for (Field field : fields) {
//            Object objeto = field.get(cliente);
//            field.setAccessible(true);
//            if (objeto == null) {
//                new ResourceNotFoundException("Os Campos devem ser todos atualizados");
//            }
//        }
//    }

}

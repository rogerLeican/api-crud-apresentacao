package com.technocorp.apicrudapresentacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCrudApresentacaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiCrudApresentacaoApplication.class, args);
    }

}
